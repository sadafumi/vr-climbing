﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCtr : MonoBehaviour
{
    //カメラ回転制御用
    public float xSpeed = 2f;
    public float ySpeed = 2f;
    public float yMinLimit = -90f;
    public float yMaxLimit = 90f; 
    private bool isRotate;
    private float x;
    private float y;

    //カメラ移動制御用
    public float moveSpeed = 2f;
    public float rotateSpeed = 2f;

    //上るフラグ
    public bool OnClimb;

    //上る処理用
    public float Move_Y;

    //マウス座標
    private Vector3 MousePosRealTime;

    //建物をキャッチした時点のマウス座標
    private Vector3 MousePosOnClimb;

    //上る時点のプレイヤー座標
    private Vector3 PlayerPosOnClimb;

    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        OnClimb = false;

        Debug.Log(Physics.gravity);
    }

    private void Update()
    {
        //上り状態ではない時(プレイヤー移動可)
        if (!OnClimb)
        {
            CameraMove();
        }

        //マウス座標を常にゲットする
        MousePosRealTime = Input.mousePosition;
    }

    void LateUpdate()
    {
        //カメラ回転更新処理
        RotateCamera();
    }


    void RotateCamera()
    {
        //カメラ回転処理
        y -= Input.GetAxis("Mouse Y") * ySpeed;
        y = Mathf.Clamp(y, yMinLimit, yMaxLimit);
        x += Input.GetAxis("Mouse X") * xSpeed;
        transform.eulerAngles = new Vector3(y, x, 0);
    }

    //判定範囲に入ったら、マウスで建物を捕まえて、上り状態に入る
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "bill")
        {
            //捕まったら
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("ビルを捕まえている");
                if(!OnClimb)
                {
                    //フラグ切換
                    OnClimb = true;

                    //重力をなしにする
                    Physics.gravity = new Vector3(0, 0, 0);

                    //マウス座標をゲット
                    MousePosOnClimb = MousePosRealTime;

                    //登ろうとした瞬間にプレイヤー座標をゲット
                    PlayerPosOnClimb = this.transform.position;
                }
            }
            //上る中
            else if(Input.GetMouseButton(1))
            {
                //捕まえて、上に登ろうとしたら
                if (MousePosRealTime.y < MousePosOnClimb.y && this.transform.position.y - PlayerPosOnClimb.y <=5.0f)
                {
                    this.gameObject.transform.position += new Vector3(0, 0.1f, 0);
                }
            }
            else if (Input.GetMouseButtonUp(1))
            {
                Debug.Log("ビルから手を離した");
                OnClimb = false;

                //上るスピードをリセット
                Move_Y = 0;

                //重力を戻に戻す
                Physics.gravity = new Vector3(0, -9.8f, 0);

                MousePosRealTime = Input.mousePosition;
                PlayerPosOnClimb = this.transform.position;
            }
        }
    }

    //建物を捕まえるかどうか判定する
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bill")
        {
            Debug.Log("捕まえる範囲に入りました");
        }
    }

    //捕まえる範囲から離した。
    private void OnTriggerExit(Collider other)
    {
        if(other.tag=="Bill")
        {
            Debug.Log("捕まえる範囲から離した");
            OnClimb = false;
            //上るスピードをリセット
            Move_Y = 0;
            //重力を戻に戻す
            Physics.gravity = new Vector3(0, 9.8f, 0);
        }
    }

    //カメラ移動関数
    private void CameraMove()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        if (h != 0 || v != 0)
        {
            Vector3 targetDirection = new Vector3(h, 0, v);
            float y = Camera.main.transform.rotation.eulerAngles.y;
            targetDirection = Quaternion.Euler(0, y, 0) * targetDirection;

            transform.Translate(targetDirection * Time.deltaTime * moveSpeed, Space.World);
        }
    }
}
