﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Height : MonoBehaviour
{
    [SerializeField]
    private Text heightText;
    private float height;
    private float oldHeight;

    // Start is called before the first frame update
    void Start()
    {
        height = 0f;
        oldHeight = 0f;
        heightText = GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        height = this.gameObject.transform.position.y;
        if (height != oldHeight)
        {
            heightText.text = height.ToString("00") + "m";
        }
        oldHeight = height;
    }
}
